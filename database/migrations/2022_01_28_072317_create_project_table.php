<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project', function (Blueprint $table) {
<<<<<<< HEAD:database/migrations/2022_01_28_072317_create_project_table.php
            $table->bigIncrements('id');
            $table->string('judul');
            $table->string('deskripsi');
            $table->date('tanggal');
            $table->string('budget');
            $table->string('durasi');
            $table->string('kategori');
            $table->string('owner');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('update_at')->nullable();

=======
            $table->increments('id');
            $table->char('judul');
            $table->char('deskripsi');
            $table->integer('budget');
            $table->integer('durasi');
            $table->char('kategori');
            $table->char('owner');
            $table->timestamps();
>>>>>>> becc0a840e39129b4b8d478a6bbf85e37a556f08:database/migrations/2022_01_27_131111_create_project_table.php
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project');
    }
}
