
@extends('layouts.master')

@section('title')
    Dari Rumah
@endsection



@section('content')

@include('sweetalert::alert')
    <div class="jumbotron text-center text-white m-t-0" style="background-image: url('jt_welcome2.jpg');height: 500px;">
        <div class="mask" style="background-color: rgba(0,0,0,0.6);height:500px;">
            <div class="d-flex justify-content-center align-items-center h-100 ">
                <div>
                    <h1 class="display-4">Hai,
                        @auth
                        {{ Auth::user()->name }}
                        @endauth
                        @guest
                        Freelancer
                        @endguest
                        </h1>
                    <p class="lead">Mari dapatkan banyak tawaran proyek freelance di Indonesia</p>
                    <hr class="my-4">
                    @auth
                    <p>Silahkan explore projects di sini <b>gratis</b> sekarang !</p>
                    <a class="btn btn-outline-light btn-lg" href="/projects/" role="button">Projects</a>
                    @endauth
                    @guest
                    <p>Masih terdapat banyak ruang bagi Anda, daftar <b>gratis</b> sekarang !</p>
                    <a class="btn btn-outline-light btn-lg" href="#" role="button">Masuk</a>
                    <a class="btn btn-primary btn-lg" href="/daftar" role="button">Daftar</a>
                    @endguest
                </div>
            </div>
        </div>
    </div>

    <section id="sec-about" class="sec-about pt-5 pb-5">
        <div class="container">
          <div class="row justify-content-center text-center">
            <div class="col-md-10 col-lg-8">
              <h4 class="display-7">Temukan Beragam Proyek di Indonesia</h4>
              <p class="lead mt-4 mb-4">Dari Rumah memberikan sebuah wadah untuk project owner menempatkan beragam proyek mereka yang dapat dikerjakan oleh freelancer seperti pada bidang</p>
            </div>
          </div>

          <div class="row mt-4">
            <div class="col-sm-4">
              <h5 class="display-7">Pengembangan IT</h5>
              <hr />
              <a href="" class="btn btn-outline-secondary btn-sm mb-2">Fullstack Development</a>
              <a href="" class="btn btn-outline-secondary btn-sm mb-2">Frondent Development</a>
              <a href="" class="btn btn-outline-secondary btn-sm mb-2">Backend Development</a>
              <a href="" class="btn btn-outline-secondary btn-sm mb-2">Mobile Development</a>
              <a href="" class="btn btn-outline-secondary btn-sm mb-2">Quality Assurance</a>
              <a href="" class="btn btn-outline-secondary btn-sm mb-2">UI/UX Design</a>
            </div>

            <div class="col-sm-4">
                <h5 class="display-7">Desain & Multimedia</h5>
                <hr />
                <a href="" class="btn btn-outline-secondary btn-sm mb-2">Grapich Design</a>
                <a href="" class="btn btn-outline-secondary btn-sm mb-2">Illustration</a>
                <a href="" class="btn btn-outline-secondary btn-sm mb-2">Animation</a>
                <a href="" class="btn btn-outline-secondary btn-sm mb-2">Photography</a>
                <a href="" class="btn btn-outline-secondary btn-sm mb-2">Photo Editing</a>
                <a href="" class="btn btn-outline-secondary btn-sm mb-2">Videography</a>
                <a href="" class="btn btn-outline-secondary btn-sm mb-2">Video Editing</a>
                <a href="" class="btn btn-outline-secondary btn-sm mb-2">Voice Over</a>
              </div>

              <div class="col-sm-4">
                <h5 class="display-7">Marketing & Writing</h5>
                <hr />
                <a href="" class="btn btn-outline-secondary btn-sm mb-2">Digital Marketing</a>
                <a href="" class="btn btn-outline-secondary btn-sm mb-2">Search Engine Optimization</a>
                <a href="" class="btn btn-outline-secondary btn-sm mb-2">Social Media Marketing</a>
                <a href="" class="btn btn-outline-secondary btn-sm mb-2">Copywriting</a>
                <a href="" class="btn btn-outline-secondary btn-sm mb-2">Content Writing</a>
                <a href="" class="btn btn-outline-secondary btn-sm mb-2">Translation</a>
                <a href="" class="btn btn-outline-secondary btn-sm mb-2">Career Writing</a>
              </div>
          </div>
        </div>
      </section>

@endsection
