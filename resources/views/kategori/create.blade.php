@extends('layouts.admin_master')

@section('title')
    Tambah Kategori
@endsection

@section('content')
<form action="/kategori" method="POST">
    @csrf
    <div class="form-group">
        <label for="nama_kategori">Nama Kategori</label>
        <input type="text" class="form-control" name="nama_kategori" id="nama_kategori" placeholder="Masukkan Nama Kategori">
        @error('nama_kategori')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
   
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection