@extends('layouts.admin_master')

@section('title')
    Edit Kategori {{$kategori->nama_kategori}}
@endsection

@section('content')
<form action="/kategori/{{$kategori->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="nama_kategori">Nama Kategori</label>
        <input type="text" value="{{$kategori->nama_kategori}}" class="form-control" name="nama_kategori" id="nama_kategori" placeholder="Masukkan Nama Kategori">
        @error('nama_kategori')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-success">Update</button>
</form>
@endsection