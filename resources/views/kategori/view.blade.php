@extends('layouts.admin_master')

@section('title')
    List Kategori
@endsection

@section('content')
<a href="/kategori/create" class="btn btn-primary">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama Kategori</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
                @forelse ($kategori as $key=>$item)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$item->nama_kategori}}</td>
                        <td>
                           
                            <form action="/kategori/{{$item->id}}" method="POST">
                                @csrf
                                @method('delete')
                                <a href="/kategori/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                                <a href="/kategori/{{$item->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                                <input Type="submit" class="btn btn-danger btn-sm" value="delete">
                            </form>
                    </tr>
                @empty
                     <tr>
                         <td>TIdak ada Data</td>
                     </tr>
                @endforelse              
            </tbody>
        </table>
@endsection