@extends('layouts.master')

@section('title')
    Proposal
@endsection

@section('content')

@include('sweetalert::alert')

<br>
<br>
    <div class="container mb-5">
        <form action="/kirimproposal" method="POST">
        @csrf
        <h3><center>Proposal</center></h3>
        <hr>
        <br>
            <div class="col-md-6">
            <div class="form-group">
                <label for="judul">Judul</label>
                <h4 value="judul">{{$project->judul}}</h4>
                <input hidden value="{{$project->judul}}" type="text" name="judul" id="judul">
            </div>
            </div>
            <!--  col-md-6   -->
            <div class="col-md-6">
            <div class="form-group">
                <label for="budget">Budget/ Harga</label>
                <h5 value="budget">{{$project->budget}}</h5>
                <input hidden value="{{$project->budget}}" type="integer" name="budget" id="budget">
            </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="company">Hari selesai</label>
                    <h5 value="durasi">{{$project->durasi}} Hari</h5>
                    <input hidden value="{{$project->durasi}}" type="integer" name="durasi" id="durasi">
                </div>
                </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="company">Owner</label>
                    <h5 name="owner">{{$project->owner}}</h5>
                    <input hidden value="{{$project->owner}}" type="text" name="owner" id="owner">
                </div>
                </div>
        <br>
        <label class="col-xs-3 control-label">Deskripsi</label>
        <div class="col-xs-9">
            <p value="deskripsi">{{$project->deskripsi}}</p>
            <input hidden value="{{$project->deskripsi}}" type="text" name="deskripsi" id="deskripsi">
        </div>
        <br>
        <hr>
        <br>
        <label class="col-xs-3 control-label">Rincian pengajuan :</label>
        <div class="col-xs-9">
            <textarea class="form-control" name="isiproposal" id="isiproposal" rows="8"></textarea>
        </div>

        <br><br>
        <label for="persetujuan"><b>Dengan ini Anda telah menyetujui ketentuan kami.</b></label>
        <div class="checkbox">
            <label>
            <input class="form-check-input" type="checkbox" name="setuju" value="ya" id="persetujuan">
            <label class="form-check-label" for="persetujuan">Ya, Saya setuju.</label>
            </label>
        </div>
        <br>


        <button type="submit" class="btn btn-primary">Kirim</button>
        </form>
    </div>

@endsection
