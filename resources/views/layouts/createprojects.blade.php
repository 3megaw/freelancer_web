@extends('layouts.master')

@section('title')
    Create Projects
@endsection

@section('content')

@include('sweetalert::alert')
<form action="/post" method="POST">
    @csrf
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan Title">
        @error('title')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">body</label>
        <input type="text" class="form-control" name="deskripsi" id="deskripsi" placeholder="Masukkan Body">
        @error('body')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

<br>
<br>
<br>
    <div class="container mb-5">
        <form>
        <h3>Buat Proyek</h3>
        <hr>


<br>
<br>
    <div class="container mb-5">
        <form action="/buatproyek" method="POST">
        @csrf
        <h3><center>Buat Proyek</center></h3>
        <hr>
        <br>
        <div class="row">
            <div class="col-md-6">
            <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text" class="form-control" placeholder="" name="judul" id="judul">
            </div>
            </div>
            <!--  col-md-6   -->
            <div class="col-md-6">
            <div class="form-group">
                <label for="budget">Budget/ Harga</label>
                <input type="number" class="form-control" placeholder="" name="budget" id="budget">
            </div>
            </div>
            <!--  col-md-6   -->
        </div>
        <br>
        <label class="col-xs-3 control-label">Deskripsi</label>
        <div class="col-xs-9">

            <div style="border: 1px solid #e5e5e5; height: 200px; overflow: auto; padding: 10px;">

            </div>

            <textarea class="form-control" name="deskripsi" id="deskripsi" rows="8"></textarea>

            <textarea class="form-control" name="deskripsi" id="deskripsi" rows="8"></textarea>

        </div>
        <br>

        <div class="row">
            <div class="col-md-6">
            <div class="form-group">
                <label for="company">Hari selesai</label>

                <input type="text" class="form-control" placeholder="" id="company">

                <input type="number" name="durasi" class="form-control" placeholder="" id="durasi">
            </div>
            </div>
            <!--  col-md-6   -->


            <div class="col-md-6">
            <div class="form-group">
                <label class="" for="customFile">Berkas pendukung</label>
                <input type="file" class="form-control" id="customFile" />
            </div>
            </div>
            <!--  col-md-6   -->
        </div>
        <!--  row   -->

        <br>

        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1" />
            <label class="form-check-label" for="inlineCheckbox1">Fullstack Development</label>
          </div>

          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" />
            <label class="form-check-label" for="inlineCheckbox2">Frontend Development</label>
          </div>

          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3"/>
            <label class="form-check-label" for="inlineCheckbox3">Backend Development</label>
          </div>

          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1" />
            <label class="form-check-label" for="inlineCheckbox1">Fullstack Development</label>
          </div>

          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" />
            <label class="form-check-label" for="inlineCheckbox2">Frontend Development</label>
          </div>

          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3"/>
            <label class="form-check-label" for="inlineCheckbox3">Backend Development</label>
          </div>

          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1" />
            <label class="form-check-label" for="inlineCheckbox1">Fullstack Development</label>
          </div>

          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" />
            <label class="form-check-label" for="inlineCheckbox2">Frontend Development</label>
          </div>

          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3"/>
            <label class="form-check-label" for="inlineCheckbox3">Backend Development</label>
          </div>

          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1" />
            <label class="form-check-label" for="inlineCheckbox1">Fullstack Development</label>
          </div>

          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" />
            <label class="form-check-label" for="inlineCheckbox2">Frontend Development</label>
          </div>

          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3"/>
            <label class="form-check-label" for="inlineCheckbox3">Backend Development</label>
          </div>

          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1" />
            <label class="form-check-label" for="inlineCheckbox1">Fullstack Development</label>
          </div>

          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" />
            <label class="form-check-label" for="inlineCheckbox2">Frontend Development</label>
          </div>

          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3"/>
            <label class="form-check-label" for="inlineCheckbox3">Backend Development</label>
          </div>

          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1" />
            <label class="form-check-label" for="inlineCheckbox1">Fullstack Development</label>
          </div>

          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" />
            <label class="form-check-label" for="inlineCheckbox2">Frontend Development</label>
          </div>

          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3"/>
            <label class="form-check-label" for="inlineCheckbox3">Backend Development</label>
          </div>
            <input class="form-check-input" type="checkbox" id="kategori" name="kategori" value="{{ $category->nama_kategori }}" />
            <label class="form-check-label" for="kategori">{{ $category->nama_kategori}}</label>
        </div>
        @endforeach
        <br><br>
        <label for="newsletter">Degan ini Anda telah menyetujui ketentuan</label>
        <div class="checkbox">
            <label>
            <input type="checkbox" value="Sure!" id="newsletter"> Sure!
        <br><br>
        <label for="persetujuan"><b>Degan ini Anda telah menyetujui ketentuan kami.</b></label>
        <div class="checkbox">
            <label>
            <input class="form-check-input" type="checkbox" name="setuju" value="ya" id="persetujuan">
            <label class="form-check-label" for="persetujuan">Ya, Saya setuju.</label>
            </label>
        </div>
        <br>

        <button type="submit" class="btn btn-primary">Buat</button>
        </form>
    </div>

@endsection
