@extends('layouts.master')

@section('title')
    Detail
@endsection

@section('content')

<div class="jumbotron text-center text-white m-t-0" style="background-image: url('jt_welcome2.jpg');height: 250px;">
    <div class="mask" style="background-color: rgba(0,0,0,0.6);height:250px;">
        <div class="d-flex justify-content-center align-items-center h-100 ">
            <div>
                <p class="lead">Punya banyak proyek tapi bingung tak ada yang ngerjain ?</p>
                <hr class="my-4">
                @auth
                <p>Posting proyekmu sekarang dan dapatkan talenta yang sesuai !</p>
                <a class="btn btn-outline-light btn" href="#" role="button">Buat Proyek</a>
                @endauth
                @guest
                <p>Masih terdapat banyak ruang bagi Anda, daftar <b>gratis</b> sekarang !</p>
                <a class="btn btn-outline-light btn-lg" href="#" role="button">Masuk</a>
                <a class="btn btn-primary btn-lg" href="/daftar" role="button">Daftar</a>
                @endguest

            </div>
        </div>
    </div>
</div>

<body>
    <br>
    <div id="bd">
        <div class="row">
            <div class="col-md-8 ms-5">
                <h3>{{$project->judul}}</h3>
                <p>{{$project->deskripsi}}</p>
                <div class="row">
                    <div class="d-flex bd-highlight mt-1 pe-5">
                        <div class="bd-highlight">
                            <p>
                                <b>Status : </b>Aktif merekrut <br>
                                <b>Budget : </b>{{$project->budget}}<br>
                                <b>Owner : </b>{{$project->owner}}<br>
                                <b>Deadline : </b>{{$project->durasi}} hari <br>
                                <b>Lampiran : </b>
                        </div>

                        <div class="ms-auto">
                            <a href="" class="btn btn-outline-secondary btn-sm mb-2">{{$project->kategori}}</a>
                        </div>
                    </div>

    </div>
            </div>
            <div class="col-md-3 ms-5">
            <h5>Project Owner</h5>
            <div class="well well-sm">
                        <img src="http://www.freeiconspng.com/uploads/profile-icon-9.png" alt=""
                             class="img-rounded img-responsive" style="width:150px"/>
                        <h5>{{$project->owner}}</h5>
                        <small><cite title="San Francisco, USA">San Francisco, USA <i
                                class="glyphicon glyphicon-map-marker">
                        </i></cite></small>
                        <p>
                            <i class="glyphicon glyphicon-globe"></i><a href="https://developer.mozilla.org/en-US/">https://developer.mozilla.org</a>
                            <br/>
                            <i class="glyphicon glyphicon-gift"></i>January 1st, 1900</p>

                <a href="" class="btn btn-success btn-lg mb-4">Kirim Proposal</a>
            </div>
        </div>
      </div>
    </div>
</body>

@endsection
