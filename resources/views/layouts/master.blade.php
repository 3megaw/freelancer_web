

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    @stack('styles')
    <!-- Google Font: Source Sans Pro -->
    <!-- Theme style
    -->
    <link rel="stylesheet" href="{{asset('css\bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css\tambahan.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">


</head>
<body>
    @include('layouts.partial.nav')
    @yield('content')


      <section class="">
        <!-- Footer -->
        <footer class="text-black text-center text-md-start" style="background-color: rgb(241, 241, 241);">
          <!-- Grid container -->
          <div class="container p-4">
            <!--Grid row-->
            <div class="row">
              <!--Grid column-->
              <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
                <h5 class="">Dari Rumah</h5>
                <p>
                    Dari Rumah merupakan freelancer marketplace di Indonesia yang memiliki tujuan sebagai media perantara untuk dapat membantu pihak freelancer dan project owner bertemu dan menyelesaikan project dengan aman dan nyaman.
                </p>
              </div>
              <!--Grid column-->

              <!--Grid column-->
              <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                <h5 class="">Tentang Kami</h5>

                <ul class="list-unstyled mb-0">
                  <li>
                    <a href="#!" class="text-black">Link 1</a>
                  </li>
                  <li>
                    <a href="#!" class="text-black">Link 2</a>
                  </li>
                  <li>
                    <a href="#!" class="text-black">Link 3</a>
                  </li>
                  <li>
                    <a href="#!" class="text-white">Link 4</a>
                  </li>
                </ul>
              </div>
              <!--Grid column-->

              <!--Grid column-->
              <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                <h5 class="">Kontak Kami</h5>

                <ul class="list-unstyled">
                  <li>
                    <a href="#!" class="text-white">Link 1</a>
                  </li>
                  <li>
                    <a href="#!" class="text-white">Link 2</a>
                  </li>
                  <li>
                    <a href="#!" class="text-white">Link 3</a>
                  </li>
                  <li>
                    <a href="#!" class="text-white">Link 4</a>
                  </li>
                </ul>
              </div>
              <!--Grid column-->
            </div>
            <!--Grid row-->
          </div>
          <!-- Grid container -->

          <!-- Copyright -->
          <div class="text-center p-3" style="background-color: rgba(173, 173, 173, 0.2);">
            © 2022 Copyright:
            <a class=""" href="">darirumah.com</a>
          </div>
          <!-- Copyright -->
        </footer>
        <!-- Footer -->
      </section>

    <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>

    @stack('scripts')
    @include('sweetalert::alert')
</body>
</html>
