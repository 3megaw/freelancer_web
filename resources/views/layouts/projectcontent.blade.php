
@extends('layouts.master')

@section('title')
    Projects
@endsection

@section('content')
@include('sweetalert::alert')
    <div class="jumbotron text-center text-white m-t-0" style="background-image: url('jt_welcome2.jpg');height: 250px;">
        <div class="mask" style="background-color: rgba(0,0,0,0.6);height:250px;">
            <div class="d-flex justify-content-center align-items-center h-100 ">
                <div>
                    <p class="lead">Punya banyak proyek tapi bingung tak ada yang ngerjain ?</p>
                    <hr class="my-4">
                    @auth
                    <p>Posting proyekmu sekarang dan dapatkan talenta yang sesuai !</p>
                    <a class="btn btn-outline-light btn" href="#" role="button">Buat Proyek</a>
                    @endauth
                    @guest
                    <p>Masih terdapat banyak ruang bagi Anda, daftar <b>gratis</b> sekarang !</p>
                    <a class="btn btn-outline-light btn-lg" href="#" role="button">Masuk</a>
                    <a class="btn btn-primary btn-lg" href="/daftar" role="button">Daftar</a>
                    @endguest

                </div>
            </div>
        </div>
    </div>

    <div class="row mt-5 ms-3 mr-3">
        <div class="col-4">
            <div class="row container-fluid">
                <div class="col col-lg-9">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                  </div>
                  <div class="col col-lg-1">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                  </div>
            </div>
            <div class="list-group me-5 mt-2" id="list-tab" role="tablist">
                <a class="list-group-item list-group-item-action" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Home</a>
                <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">Profile</a>
                <a class="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages">Messages</a>
                <a class="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings">Settings</a>
            </div>
        </div>

        <div class="col-8">
            <div class="row container-fluid">
                <h5 class="pe-3">Mengerjakan sistem informasi puskesmas android menggunakan kotlin dan database firebase realtime database</h5>
                <div class="col col-lg-10">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                  </div>
                  <div class="col col-lg-1 mb-4">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                  </div>

                @foreach ($projects as $key=>$value)
                <a href="/detailproyek/{{$value->id}}">
                    <h5 class="pe-3">{{$value->judul}}</h5>
                  </a>
                <div>
                    <div class="bd-highlight"><b>Tanggal posting : </b>{{$value->created_at}}</p></div>
                </div>
                <p style="max-lines: 5;">
                    {{$value->deskripsi}}
                </p>
                <div>
                    <div>
                        <div class="row">
                            <div class="d-flex bd-highlight mt-1 pe-5">
                                <div class="bd-highlight">
                                    <p>
                                        <b>Status : </b>Aktif merekrut <br>
                                        <b>Budget : </b>{{$value->budget}}<br>
                                        <b>Owner : </b>Kartiko Johar <br>
                                        <b>Deadline : </b>{{$value->durasi}} hari
                                </div>

                                <div class="ms-auto bd-highlight">
                                    <a href="" class="btn btn-outline-secondary btn-sm mb-2">{{$value->kategori}}</a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="mt-1 mb-4">
                @endforeach





            </div>
        </div>
      </div>
@endsection

