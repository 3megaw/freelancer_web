@extends('layouts.admin_master')

@section('title')
   Daftar Transaksi
@endsection

@section('content')


        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">No</th>
                <th scope="col">Project</th>
                <th scope="col">User Bidder</th>
                <th scope="col">User Owner</th>
                <th scope="col">Tanggal Selesai</th>
                <th scope="col">Note</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                       
                        <form action="/cast" method="POST">
                            @csrf
                            @method('delete')
                            <input Type="submit" class="btn btn-danger btn-sm" value="delete">
                        </form>
                </tr>
                       
        </tbody>
        </table>
@endsection