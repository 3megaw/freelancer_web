@extends('layouts.admin_master')

@section('title')
   List Project
@endsection

@section('content')


        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">No</th>
                <th scope="col">Judul</th>
                <th scope="col">Deskripsi</th>
                <th scope="col">Budget</th>
                <th scope="col">Durasi</th>
                <th scope="col">Kategori</th>
                <th scope="col">Owner</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            <tbody>
                @forelse ($project as $key=>$item)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$item->judul}}</td>
                    <td>{{$item->deskripsi}}</td>
                    <td>{{$item->budget}}</td>
                    <td>{{$item->durasi}}</td>
                    <td>{{$item->kategori}}</td>
                    <td>{{$item->owner}}</td>
                    <td>
                       
                        <form action="/admin/{{$item->id}}" method="POST">
                            @csrf
                            @method('delete')
                            <input Type="submit" class="btn btn-danger btn-sm" value="delete">
                        </form>
                </tr>
                @empty
                <tr>
                    <td>TIdak ada Data</td>
                </tr>
           @endforelse                
            </tbody>  
        </tbody>
        </table>
@endsection