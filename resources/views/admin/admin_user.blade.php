@extends('layouts.admin_master')

@section('title')
   List User
@endsection

@section('content')


        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col">E-mail</th>
                <th scope="col">Bio</th>
                <th scope="col">No HP</th>
                <th scope="col">Alamat</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($users as $key=>$item)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{$item->bio}}</td>
                    <td>{{$item->nohp}}</td>
                    <td>{{$item->alamat}}</td>
                    <td>
                        <form action="/admin/{{$item->id}}" method="POST">
                            @csrf
                            @method('delete')
                        <form action="/admin" method="POST">
                            @csrf
                            @method('delete')
                            <input Type="submit" class="btn btn-danger btn-sm" value="delete">
                        </form>
                </tr>
                @empty
                <tr>
                    <td>TIdak ada Data</td>
                </tr>
           @endforelse          
        </tbody>
        </table>
@endsection