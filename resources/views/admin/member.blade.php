@extends('layouts.admin_master')

@section('title')
   List Member
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col">Bio</th>
                <th scope="col">Email</th>
                <th scope="col">no Hp</th>
                <th scope="col">Bio</th>
                <th scope="col">Alamat</th>
                <th scope="col">Foto Profil</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($member as $key=>$item)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->bio}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{$item->nohp}}</td>
                    <td>{{$item->bio}}</td>
                    <td>{{$item->alamat}}</td>
                    <td>{{$item->fotoprofil}}</td>
                    <td>
                       
                        <form action="/member/{{$item->id}}" method="POST">
                            @csrf
                            @method('delete')
                            <a href="/member/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                            <a href="/member/{{$item->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                            <input Type="submit" class="btn btn-danger btn-sm" value="delete">
                        </form>
                </tr>
                @empty
                <tr>
                    <td>TIdak ada Data</td>
                </tr>
           @endforelse             
        </tbody>
        </table>
@endsection