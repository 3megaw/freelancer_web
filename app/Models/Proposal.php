<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
    protected $table = "proposal";
    protected $fillable = ["judul", "isiproposal","deskripsi","budget", "durasi","created_at","updated_at","owner","worker"];
}
