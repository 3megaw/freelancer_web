<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    protected $table = "project";
    protected $fillable = ["judul", "deskripsi","budget","kategori", "durasi","created_at","updated_at","owner"];
    //public $timestamps =  false;
}
