<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Projects;
use App\Models\Proposal;
use Auth;
use Alert;

class ProposalController extends Controller
{
    public function show($id)
    {
        $project = Projects::find($id);
        return view('layouts.proposal', compact('project'));
    }
    public function store(Request $request){
        Proposal::create([
            'judul' => $request->judul,
            'budget' => $request->budget,
            'isiproposal' => $request->isiproposal,
            'deskripsi' => $request->deskripsi,
            'durasi' => $request->durasi,
            'worker' => Auth::user()->name,
            'owner' => $request->owner,
        ]);

        Alert::success('Proposal berhasil dikirim');
        return redirect('/projects');
    }
}
