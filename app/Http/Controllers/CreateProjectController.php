<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProjectCategory;
use App\Models\Projects;
use Auth;
use Alert;

class CreateProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Projects::orderBy('created_at','desc')->get();
        $category = ProjectCategory::all();
        //return view('layouts.createprojects', ['category' => $category]);
        return view('layouts.projectcontent', compact('projects'),['category' => $category]);

    }

    public function indexcategory()
    {
        $category = ProjectCategory::all();
        return view('layouts.createprojects', ['category' => $category]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.createprojects');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //$project = Project::create($request->all());
         Projects::create([
     		'judul' => $request->judul,
            'harga' => $request->harga,
            'budget' => $request->budget,
     		'deskripsi' => $request->deskripsi,
     		'durasi' => $request->durasi,
     		'kategori' => $request->kategori,
            'owner' => Auth::user()->name
     	]);

         Alert::success('Project berjasil diposting');
     	return redirect('/projects');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Projects::find($id);
        return view('layouts.detailproyek', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
