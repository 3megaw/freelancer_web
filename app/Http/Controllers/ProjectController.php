<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ProjectController extends Controller
{
    public function index(){
        $project = DB::table('project')->get();
        //dd($project);
        return view('admin.project', compact('project'));
    }

    public function destroy($id){
        DB::table('project')->where('id', '=', $id)->delete();

        return redirect('/admin');
    }
}
