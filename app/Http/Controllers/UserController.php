<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('admin.admin_user', compact('users'));
    }

    public function show($id)
    {
        $users = User::findorfail($id);
        return view('admin.admin_user_show', compact('users'));
    }
    public function edit($id)
    {
        $users = User::findorfail($id);
        return view('admin_user_edit', compact('film'));
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'bio' => 'required',
            'nohp' => 'required',
            'alamat' => 'required',
        ]);

        $users = User::findorfail($id);

       

            $users_data=[
                'name' => $request->name,
                'email' => $request->email,
                'password' => $request->password,
                'bio' => $request->bio,
                'nohp' => $request->nohp,
                'alamat' => $request->alamat,
            ];
            
        
        $users->update($users_data);

        return redirect('/admin/user');
    }

    public function destroy($id)
    {
        $users = User::findorfail($id);
        $users->delete();

        return redirect('/user');
    }
}
