<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ProjectCategoryController extends Controller
{
    public function create(){
        return view('kategori.create');
    }


    public function store(Request $request){
        $request->validate([
            'nama_kategori' => 'required|unique:project_categories,nama_kategori|max:255',
            
        ]);
        DB::table('project_categories')->insert(
            [
                'nama_kategori' => $request['nama_kategori'],
               
             ]
        );
        return redirect('/kategori')->with('success','Cast berhasil disimpan');
    }

    public function index(){
        $kategori = DB::table('project_categories')->get();

        return view('kategori.view', compact('kategori'));
    }

    public function show($id){
        $kategori = DB::table('project_categories')->where('id', $id)->first();
        return view('kategori.show', compact('kategori'));
    }

    public function edit($id){
        $kategori = DB::table('project_categories')->where('id', $id)->first();
        return view('kategori.edit', compact('kategori'));
    }

    public function update(Request $request, $id){
        $request->validate([
            'nama_kategori' => 'required|unique:project_categories,nama_kategori|max:255',
            
        ]);

        DB::table('project_categories')
        ->where('id', $id)
        ->update(
            [
                'nama_kategori' => $request['nama_kategori'],
            ]
        );
        return redirect('/kategori');

    }

    public function destroy($id){
        DB::table('project_categories')->where('id', '=', $id)->delete();

        return redirect('/kategori');
    }
}
