<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProjectCategoryController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Register Daftar Akun
Route::get('/daftar','DaftarController@index');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('/projects', function () {
    return view('layouts.projectcontent');
});

Route::get('/buatproyek', function () {
    return view('layouts.createprojects');
});

// Route::get('/buatproyek', function () {
//     return view('layouts.createprojects');
// });

Route::get('/projects','CreateProjectController@index');
Route::get('/buatproyek','CreateProjectController@indexcategory');
Route::post('/buatproyek','CreateProjectController@store');
Route::post('/post','CreateProjectController@store');



Route::get('/detailproyek/{project_id}', 'CreateProjectController@show');




//admin
Route::get('/admin/login', 'AdminController@index');
Route::get('/admin/register', 'AdminController@register');

//project
Route::get('/admin', 'ProjectController@index');
Route::delete('/admin/{project_id}','ProjectController@destroy');

//user
Route::get('/user','UserController@index');
Route::get('/user/{user_id}','UserController@show');
Route::get('/user/{user_id}/edit','UserController@edit');
Route::put('/user/{user_id}','UserController@update');
Route::delete('/user/{user_id}','UserController@destroy');

Route::get('/profile','ProfileController@index');

Route::post('/kirimproposal','ProposalController@store');
Route::get('/proposal/{proposal_id}', 'ProposalController@show');

Route::get('/detailproyek/{project_id}', 'CreateProjectController@show');



//admin


Route::get('/transaksi', function () {
    return view('admin.transaksi');
});


//kategori
Route::get('/kategori/create','ProjectCategoryController@create');
Route::post('/kategori','ProjectCategoryController@store');
Route::get('/kategori','ProjectCategoryController@index');
Route::get('/kategori/{cast_id}','ProjectCategoryController@show');
Route::get('/kategori/{cast_id}/edit','ProjectCategoryController@edit');
Route::put('/kategori/{cast_id}','ProjectCategoryController@update');
Route::delete('/kategori/{cast_id}','ProjectCategoryController@destroy');